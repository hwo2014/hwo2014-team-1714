package olengu.bot;

public class TestValues {
	
	public static boolean ENABLED = false;
	public static final double TURN_RADIUS_50_SPEED = 4.5 + (Math.random() * 2.0 - 1.0);
	public static final double TURN_RADIUS_100_SPEED = 6.5 + (Math.random() * 2.0 - 1.0);
	public static final double TURN_RADIUS_200_SPEED = 9.7 + (Math.random() * 2.0 - 1.0);
	public static final double BRAKING_SPEED = 0.015 + (Math.random() * 0.02 - 0.01);
	public static final double TRACTION_CONTROL_ACTIVATION_ANGLE = 25.0 + (Math.random() * (60.0 - 25.0));
	public static final double TRACTION_CONTROL_SLOWDOWN_MULTIPLIER = 1.0 + (Math.random() * 2.0);
	
	public static void dump() {
		System.out.println("TURN_RADIUS_50_SPEED: " + TURN_RADIUS_50_SPEED);
		System.out.println("TURN_RADIUS_100_SPEED: " + TURN_RADIUS_100_SPEED);
		System.out.println("TURN_RADIUS_200_SPEED: " + TURN_RADIUS_200_SPEED);
		System.out.println("BRAKING_SPEED: " + BRAKING_SPEED);
		System.out.println("TRACTION_CONTROL_ACTIVATION_ANGLE: " + TRACTION_CONTROL_ACTIVATION_ANGLE);
		System.out.println("TRACTION_CONTROL_SLOWDOWN_MULTIPLIER: " + TRACTION_CONTROL_SLOWDOWN_MULTIPLIER);
	}

}
