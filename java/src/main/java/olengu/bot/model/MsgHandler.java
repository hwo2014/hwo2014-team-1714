package olengu.bot.model;

import olengu.bot.msg.Msg;

import com.google.gson.JsonElement;

public interface MsgHandler {
	
	String[] getHandledMsgTypes();

	void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent);

}
