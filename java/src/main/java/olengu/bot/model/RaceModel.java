package olengu.bot.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import olengu.bot.model.CarModel.TurboData;
import olengu.bot.msg.Msg;
import olengu.bot.msg.ThrottleMsg;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class RaceModel {
	
	private final Map<String, CarModel> carModels = new HashMap<>(); // Car name => model
	private PieceModel[] pieceModels;
	private double[] laneDistanceFromCenter;
	private boolean gameStarted;
	private String carName;
	
	public Set<MsgHandler> getMsgHandlers() {
		HashSet<MsgHandler> handlers = new HashSet<>();
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Examples: 
				// {"msgType":"join","data":{"name":"Olengu01","key":"EqDnQqsGInogew"}}
				// {"msgType":"joinRace","data":{"botId":{"name":"Olengu","key":"EqDnQqsGInogew"},"trackName":"keimola","carCount":1}}
				// {"msgType":"lapFinished","data":{"car":{"name":"Olengu03","color":"red"},"lapTime":{"lap":2,"ticks":695,"millis":11583},"raceTime":{"laps":3,"ticks":2102,"millis":35033},"ranking":{"overall":1,"fastestLap":1}},"gameId":"1ad7302f-f341-48b5-8f48-929f4c614df6"}
				// {"msgType":"gameEnd","data":{"results":[{"car":{"name":"Olengu","color":"red"},"result":{"laps":3,"ticks":1581,"millis":26350}}],"bestLaps":[{"car":{"name":"Olengu","color":"red"},"result":{"lap":1,"ticks":513,"millis":8550}}]},"gameId":"7f9f09e3-fdd0-49f1-b5a3-f5dd2b211046"}
				// {"msgType":"tournamentEnd","data":null,"gameId":"7f9f09e3-fdd0-49f1-b5a3-f5dd2b211046"}
				return new String[] { "join", "joinRace", "lapFinished", "gameEnd", "tournamentEnd" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				System.out.println(gameTick + "| Event: " + msgType + " - data: " + data);
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"yourCar","data":{"name":"Olengu01","color":"red"},"gameId":"eb2f3469-8822-4c67-80fa-251eddb533d8"}
				return new String[] { "yourCar" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				carName = joData.get("name").getAsString();
				System.out.println(gameTick + "| My car is \"" + carName + "\"");
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"Olengu01","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"eb2f3469-8822-4c67-80fa-251eddb533d8"}
				return new String[] { "gameInit" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				System.out.println(gameTick + "| Game initialization");
				JsonObject joData = data.getAsJsonObject();
				JsonObject joRace = joData.get("race").getAsJsonObject();
				JsonObject joTrack = joRace.get("track").getAsJsonObject();
				// Parse track pieces
				JsonArray jaPieces = joTrack.get("pieces").getAsJsonArray();
				pieceModels = new PieceModel[jaPieces.size()];
				for (int i = 0; i < jaPieces.size(); i++) {
					pieceModels[i] = PieceModel.create(jaPieces.get(i).getAsJsonObject(), i);
					System.out.println(pieceModels[i]);
				}
				// Parse lanes, example: "lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}]
				JsonArray jaLanes = joTrack.get("lanes").getAsJsonArray();
				laneDistanceFromCenter = new double[jaLanes.size()];
				for (int i = 0; i < jaLanes.size(); i++) {
					JsonObject joLane = jaLanes.get(i).getAsJsonObject();
					laneDistanceFromCenter[joLane.get("index").getAsInt()] = joLane.get("distanceFromCenter").getAsDouble();
				}
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				return new String[] { "gameStart" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				System.out.println(gameTick + "| Game start");
				gameStarted = true;
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"carPositions","data":[{"id":{"name":"Olengu01","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"eb2f3469-8822-4c67-80fa-251eddb533d8","gameTick":1}
				return new String[] { "carPositions" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				// Get the previous speed of my car if possible
				double previousMyCarSpeed = -1.0;
				boolean previousMyCarSpeedAccurate = false;
				boolean myCarTurboStarted = false;
				CarModel myCar = getMyCar();
				if (myCar != null) { 
					previousMyCarSpeed = myCar.getSpeed();
					previousMyCarSpeedAccurate = myCar.isSpeedAccurate();
					myCarTurboStarted = myCar.getTurboData() != null && TurboData.Status.Started.equals(myCar.getTurboData().getStatus());
				}
				// Update the race model with the car positions
				JsonArray jaData = data.getAsJsonArray();
				for (int i = 0; i < jaData.size(); i++) {
					JsonObject joCar = jaData.get(i).getAsJsonObject();
					JsonObject joCarID = joCar.get("id").getAsJsonObject(); 
					String name = joCarID.get("name").getAsString();
					CarModel carModel = carModels.get(name);
					if (carModel == null) {
						carModel = new CarModel(name);
						carModels.put(name, carModel);
					}
					carModel.update(joCar, gameTick);
				}
				// Update the acceleration model if possible 
				if (gameTick != null && myCar != null && previousMyCarSpeedAccurate && previousMyCarSpeed > 0.0 && myCar.isSpeedAccurate()) {
					if (previousMyCarSpeed > -0.5 && lastMsgSent != null && lastMsgSent instanceof ThrottleMsg) {
						ThrottleMsg throttleMsg = (ThrottleMsg) lastMsgSent;
						if (!myCarTurboStarted && Math.abs(1.0 - throttleMsg.value) < 0.00001) {
							CarModel.ACCEL_ON_MAX_THROTTLE.addSample(previousMyCarSpeed, myCar.getSpeed() - previousMyCarSpeed);
						} else if (Math.abs(0.0 - throttleMsg.value) < 0.00001) {
							CarModel.ACCEL_ON_MIN_THROTTLE.addSample(previousMyCarSpeed, myCar.getSpeed() - previousMyCarSpeed);
						}
					}
				}
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"crash","data":{"name":"Olengu","color":"red"},"gameId":"b5df7e50-7a45-43a3-a224-759b8ad3f439","gameTick":97}
				return new String[] { "crash" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				String carName = joData.get("name").getAsString();
				if (carName.equals(RaceModel.this.carName)) {
					System.err.println(gameTick + "| CRASHED!!!");
				} else {
					System.out.println(gameTick + "| Opponent car \"" + carName + "\" crashed");
				}
				CarModel carModel = carModels.get(carName);
				carModel.setCrashed(true);
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"spawn","data":{"name":"Olengu","color":"red"},"gameId":"b5df7e50-7a45-43a3-a224-759b8ad3f439","gameTick":497}
				return new String[] { "spawn" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				String carName = joData.get("name").getAsString();
				if (carName.equals(RaceModel.this.carName)) {
					System.out.println(gameTick + "| SPAWNED");
				} else {
					System.out.println(gameTick + "| Opponent car \"" + carName + "\" spawned");
				}
				CarModel carModel = carModels.get(carName);
				carModel.setCrashed(false);
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"finish","data":{"name":"Olengu03","color":"red"},"gameId":"1ad7302f-f341-48b5-8f48-929f4c614df6"}
				return new String[] { "finish" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				String carName = joData.get("name").getAsString();
				if (carName.equals(RaceModel.this.carName)) {
					System.out.println(gameTick + "| FINISH LINE REACHED");
				} else {
					System.out.println(gameTick + "| Opponent car \"" + carName + "\" reached the finish line");
				}
				CarModel carModel = carModels.get(carName);
				carModel.setFinishReached(true);
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"turboAvailable","data":{"turboDurationMilliseconds":500.0,"turboDurationTicks":30,"turboFactor":3.0},"gameId":"e2e0e184-bc27-4a1c-9647-8a817454229e","gameTick":1201}
				return new String[] { "turboAvailable" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				long turboDurationTicks = joData.get("turboDurationTicks").getAsLong();
				double turboFactor = joData.get("turboFactor").getAsDouble();
				// The same turbo is given to all the cars at the same time
				for (CarModel carModel : carModels.values()) {
					carModel.setTurboAvailable(turboDurationTicks, turboFactor, gameTick);
				}
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"turboStart","data":{"name":"Olengu","color":"red"},"gameId":"ca78a2a4-a891-48a6-b0f5-e423b9b9a51a","gameTick":665}
				return new String[] { "turboStart" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				String carName = joData.get("name").getAsString();
				CarModel carModel = carModels.get(carName);
				carModel.setTurboStarted(gameTick);
			}
		});
		
		handlers.add(new MsgHandler() {
			@Override
			public String[] getHandledMsgTypes() {
				// Example: {"msgType":"turboEnd","data":{"name":"Olengu","color":"red"},"gameId":"ca78a2a4-a891-48a6-b0f5-e423b9b9a51a","gameTick":696}
				return new String[] { "turboEnd" };
			}
			@Override
			public void onMsg(String msgType, JsonElement data, Long gameTick, Msg lastMsgSent) {
				JsonObject joData = data.getAsJsonObject();
				String carName = joData.get("name").getAsString();
				CarModel carModel = carModels.get(carName);
				carModel.setTurboEnded(gameTick);
			}
		});
		
		return handlers;
	}
	
	public boolean isGameStarted() {
		return gameStarted;
	}
	
	public CarModel getMyCar() {
		return carModels.get(carName);
	}
	
	public double getOptimalSpeed(int pieceIndex, double inPieceDistance, CarModel carModel) {
		double optimalSpeed = Double.MAX_VALUE;
		double dist = inPieceDistance;
		for (int i = 0; i < pieceModels.length; i++) { // Look ahead in the next pieces
			PieceModel piece = getPieceModel(pieceIndex + i);
			optimalSpeed = Math.min(optimalSpeed, piece.getOptimalSpeed(dist, this, carModel));
			dist -= piece.getLength();
		}
		return optimalSpeed;
	}
	
	public int getPieceModelCount() {
		return pieceModels.length;
	}
	
	public PieceModel getPieceModel(int pieceIndex) {
		int l = pieceModels.length;
		return pieceModels[(pieceIndex % l + l) % l];
	}
	
	public int getLaneCount() {
		return laneDistanceFromCenter.length;
	}
	
	public double getLaneDistanceFromCenter(int laneIndex) {
		return laneDistanceFromCenter[laneIndex];
	}
	
	public Set<CarModel> getOpponentCarsOnPiece(int pieceIndex) {
		Set<CarModel> result = new HashSet<>();
		int l = pieceModels.length;
		for (CarModel carModel : carModels.values()) {
			if (carModel.name.equals(carName)) { continue; }
			if ((pieceIndex % l + l) % l == (carModel.getPieceIndex() % l + l) % l) {
				result.add(carModel);
			}
		}
		return result;
	}	
	
}
