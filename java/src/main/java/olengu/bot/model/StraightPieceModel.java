package olengu.bot.model;

import com.google.gson.JsonObject;

public class StraightPieceModel extends PieceModel {
	
	public final double length;

	protected StraightPieceModel(JsonObject joPiece, int index) {
		super(joPiece, index);
		length = joPiece.get("length").getAsDouble();
	}

	@Override
	public String toString() {
		return "Straight [length=" + length + ", switchLane=" + switchLane + "]";
	}
	
	@Override
	public double getLength() {
		return length;
	}
	
	@Override
	public double getOptimalSpeed(double carDistanceFromPieceStart, RaceModel raceModel, CarModel carModel) {
		return 1000.0; // We won't ever reach this speed
	}

}
