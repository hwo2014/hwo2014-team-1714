package olengu.bot.model;

import olengu.bot.TestValues;
import olengu.bot.msg.SwitchLaneMsg.Direction;

import com.google.gson.JsonObject;

public class TurnPieceModel extends PieceModel {
	
	public final double radius;
	public final double angle;

	protected TurnPieceModel(JsonObject joPiece, int index) {
		super(joPiece, index);
		radius = joPiece.get("radius").getAsDouble();
		angle = joPiece.get("angle").getAsDouble();
	}

	@Override
	public String toString() {
		return "Turn [radius=" + radius + ", angle=" + angle + ", switchLane=" + switchLane + "]";
	}
	
	public double getLength() {
		//TODO adjust for lane
		return (2.0 * Math.PI * radius) * Math.abs(angle) / 360.0;
	}
	
	@Override
	public double getOptimalSpeed(double carDistanceFromPieceStart, RaceModel raceModel, CarModel carModel) {
		
		int laneIndex = carModel.getEndLaneIndex();
		if (Direction.Left.equals(carModel.getSelectedDirection())){ laneIndex--; }
		if (Direction.Right.equals(carModel.getSelectedDirection())){ laneIndex++; }
		laneIndex = Math.max(0, Math.min(raceModel.getLaneCount() - 1, laneIndex));
		double laneDist = raceModel.getLaneDistanceFromCenter(laneIndex);
		
		double realRadius = radius + laneDist * (-Math.signum(angle));
		double targetSpeed = CarModel.getOptimalTurnSpeed(realRadius);
		
		// Traction control
		{
			final double tcActivAngle = TestValues.ENABLED ? TestValues.TRACTION_CONTROL_ACTIVATION_ANGLE : 35.0;
			double tcMult = TestValues.ENABLED ? TestValues.TRACTION_CONTROL_SLOWDOWN_MULTIPLIER : 2.0;
			double carAngle = carModel.getAngle();
			if (Math.abs(carAngle) > tcActivAngle) {
				// Traction control
				double slowingPerc =  1.0 - ((Math.abs(carAngle) - tcActivAngle) / tcActivAngle) * tcMult;
				if (slowingPerc < 0.0) { slowingPerc = 0.0; }
				targetSpeed *= slowingPerc;
			} 
		}
				
		if (carDistanceFromPieceStart < 0.0) {
			// We are approaching the turn (not yet reached it)
			final double spannometricBrakingSpeed = TestValues.ENABLED ? TestValues.BRAKING_SPEED : 0.009;
			return targetSpeed - carDistanceFromPieceStart * spannometricBrakingSpeed;
		}
		double pieceLength = getLength();
		if (carDistanceFromPieceStart <= pieceLength) {
			// The car is currently in this piece
			PieceModel nextPiece = raceModel.getPieceModel(index + 1);
			if (nextPiece instanceof StraightPieceModel) {
				if (carModel.getInPieceDistance() > pieceLength - 40.0) {
					if (carModel.getDrift() < 20.0 && carModel.getDriftSpeed() < 0.0) {
						double turnExitBoost = 1.0 - carModel.getDriftSpeed() * 10.0;
//System.out.println("Turn exit boost: " + turnExitBoost + " " + carModel.getDriftSpeed());
						targetSpeed *= turnExitBoost; 
					}
				}
			}
			return targetSpeed;
		}
		return 1000.0; // We won't ever reach this speed
	}

}
