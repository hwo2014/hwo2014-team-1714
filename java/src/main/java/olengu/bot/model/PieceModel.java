package olengu.bot.model;

import com.google.gson.JsonObject;

public abstract class PieceModel {
	
	public final int index;
	public final boolean switchLane;
	
	protected PieceModel(JsonObject joPiece, int index) {
		this.index = index;
		switchLane = joPiece.has("switch") && joPiece.get("switch").getAsBoolean();
	}
	
	public static PieceModel create(JsonObject joPiece, int index) {
		return joPiece.has("radius") ? new TurnPieceModel(joPiece, index) : new StraightPieceModel(joPiece, index);
	}

	@Override
	public String toString() {
		return "PieceModel [switchLane=" + switchLane + "]";
	}
	
	public abstract double getLength();
	
	public abstract double getOptimalSpeed(double carDistanceFromPieceStart, RaceModel raceModel, CarModel carModel);

}
