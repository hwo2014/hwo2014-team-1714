package olengu.bot.model;

import olengu.bot.TestValues;
import olengu.bot.model.CarModel.TurboData.Status;
import olengu.bot.msg.SwitchLaneMsg.Direction;
import olengu.bot.utils.SimpleLinearFitter;

import com.google.gson.JsonObject;

public class CarModel {
	
	/** Gives you the acceleration you'd get if you apply the maximum throttle, given the current speed. **/
	public static final SimpleLinearFitter ACCEL_ON_MAX_THROTTLE = new SimpleLinearFitter("ACCEL_ON_MAX_THROTTLE", 5, 1000, 0.5, -0.0202023, 0.20202);
	
	/** Gives you the negative acceleration you'd get if you apply no throttle, given the current speed. **/
	public static final SimpleLinearFitter ACCEL_ON_MIN_THROTTLE = new SimpleLinearFitter("ACCEL_ON_MIN_THROTTLE", 5, 1000, 0.1, -0.0192261, - 0.00605655);
	
	public final String name;
	
	private long updatedAtGameTick;
	private double angle;
	private double driftSpeed;
	private double maxAbsAngle;
	private int pieceIndex;
	private double inPieceDistance;
	private double speed;
	private boolean speedAccurate;
	private boolean crashed;
	private boolean finishReached;
	private int startLaneIndex;
	private int endLaneIndex;
	Direction selectedDirection;
	private TurboData turboData; // The current turbo
	private TurboData nextTurboData; // You may receive (and later use) a turbo even while you are already using a turbo
	
	public static class TurboData {
		public enum Status { Available, Started }
		public final Long turboDurationTicks;
		public final Double turboFactor;
		public final Long availableSinceGameTick;
		
		private Status status = Status.Available;
		private Long startedSinceGameTick;
		
		public TurboData(long turboDurationTicks, double turboFactor, Long availableSinceGameTick) {
			this.turboDurationTicks = turboDurationTicks;
			this.turboFactor = turboFactor;
			this.availableSinceGameTick = availableSinceGameTick;
		}
		
		public Status getStatus() {
			return status;
		}
		
		public Long getStartedSinceGameTick() {
			return startedSinceGameTick;
		}

		@Override
		public String toString() {
			return "TurboData [turboDurationTicks=" + turboDurationTicks + ", turboFactor=" + turboFactor + ", availableSinceGameTick=" + availableSinceGameTick
					+ ", status=" + status + ", startedSinceGameTick=" + startedSinceGameTick + "]";
		}
	}
	
	public CarModel(String name) {
		this.name = name;
	}
	
	/**
	 * Example JSON:
	 * {"id":{"name":"Olengu01","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}
	 */
	void update(JsonObject joCar, Long gameTick) {
		double oldAngle = angle;
		angle = joCar.get("angle").getAsDouble();
		double absAngle = Math.abs(angle);
		driftSpeed = absAngle - Math.abs(oldAngle);
		if (absAngle > maxAbsAngle) { maxAbsAngle = absAngle; }
		
		JsonObject joPiecePosition = joCar.get("piecePosition").getAsJsonObject();
		int prevPieceIndex = pieceIndex;
		pieceIndex = joPiecePosition.get("pieceIndex").getAsInt();
		double prevInPieceDistance = inPieceDistance;
		inPieceDistance = joPiecePosition.get("inPieceDistance").getAsDouble();
		
		JsonObject joLane = joPiecePosition.get("lane").getAsJsonObject();
		startLaneIndex = joLane.get("startLaneIndex").getAsInt();
		endLaneIndex = joLane.get("endLaneIndex").getAsInt();
		
		// Retrieve speed if possible
		if (gameTick != null) {
			if (gameTick.longValue() == updatedAtGameTick + 1L) {
				if (prevPieceIndex == pieceIndex ) {
					speed = inPieceDistance - prevInPieceDistance;
					speedAccurate = true;
				} else {
					speedAccurate = false;
				}
			} else {
				speedAccurate = false;
			}
			updatedAtGameTick = gameTick;
		} else {
			speedAccurate = false;
		}
	}

	void setTurboAvailable(long turboDurationTicks, double turboFactor, Long gameTick) {
		if (turboData != null) {
			if (TurboData.Status.Started.equals(turboData.getStatus())) {
				if (nextTurboData == null) {
					nextTurboData = new TurboData(turboDurationTicks, turboFactor, gameTick);
					System.out.println(gameTick + "| Car \"" + name + "\" turbo availabe (can be used later) " + turboData);
				}
			}
			return;
		}
		turboData = new TurboData(turboDurationTicks, turboFactor, gameTick);
		System.out.println(gameTick + "| Car \"" + name + "\" turbo availabe " + turboData);
	}
	
	void setTurboStarted(Long gameTick) {
		if (turboData == null) {
			System.err.println(gameTick + "| Car \"" + name + "\" started a turbo but didn't have it!");
			return;
		}
		if (TurboData.Status.Started.equals(turboData.getStatus())) {
			System.err.println(gameTick + "| Car \"" + name + "\" started a turbo but was already in turbo!");
			return;
		}
		turboData.status = Status.Started;
		turboData.startedSinceGameTick = gameTick;
		System.out.println(gameTick + "| Car \"" + name + "\" turbo started " + turboData);
	}
	
	void setTurboEnded(Long gameTick) {
		if (turboData == null) {
			System.err.println(gameTick + "| Car \"" + name + "\" ended a turbo but didn't have it!");
			return;
		}
		if (!TurboData.Status.Started.equals(turboData.getStatus())) {
			System.err.println(gameTick + "| Car \"" + name + "\" ended a turbo but it didn't start it!");
			return;
		}
		System.out.println(gameTick + "| Car \"" + name + "\" turbo ended");
		turboData = nextTurboData;
		nextTurboData = null;
	}
	
	void setCrashed(boolean crashed) {
		if (this.crashed != crashed) {
			turboData = null;
			nextTurboData = null;
		}
		this.crashed = crashed;
	}
	
	void setFinishReached(boolean finishReached) {
		this.finishReached = finishReached;
	}
	
	public void setSelectedDirection(Direction selectedDirection) {
		this.selectedDirection = selectedDirection;
	}
	
	public Direction getSelectedDirection() {
		return selectedDirection;
	}
	
	public double getAngle() {
		return angle;
	}

	public double getDrift() {
		return Math.abs(angle);
	}
	
	public double getDriftSpeed() {
		return driftSpeed;
	}
	
	public double getMaxAbsAngle() {
		return maxAbsAngle;
	}
	
	public int getPieceIndex() {
		return pieceIndex;
	}
	
	public double getInPieceDistance() {
		return inPieceDistance;
	}
	
	public int getStartLaneIndex() {
		return startLaneIndex;
	}
	
	public int getEndLaneIndex() {
		return endLaneIndex;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public boolean isSpeedAccurate() {
		return speedAccurate;
	}
	
	public boolean isCrashed() {
		return crashed;
	}
	
	public boolean isFinishReached() {
		return finishReached;
	}
	
	public TurboData getTurboData() {
		return turboData;
	}
	
	/**
	 * Gives you the throttle you could constantly apply to reach, after a long time, a given speed.
	 * Note that this is a slow way to reach a target speed if the current speed is far from the target speed.
	 * When the target speed if far from the current speed it's better to just apply the maximum or the minimum throttle.
	 * 
	 * @param targetSpeed The target speed you'd like to reach.
	 * @return The throttle you need to constantly apply to reach the target speed (converges slowly).
	 */
	public double getConstantThrottleForSpeed(double targetSpeed) {
		double turboFactor = 1.0;
		if (turboData != null && TurboData.Status.Started.equals(turboData.getStatus())) {
			turboFactor = turboData.turboFactor;
		}
		return targetSpeed * (0.1 / turboFactor);
	}
	
	/**
	 * 
	 * @param targetSpeed
	 * @return
	 */
	public double getAdaptiveThrottleForSpeed(double currentSpeed, double targetSpeed) {
		double turboFactor = 1.0;
		if (turboData != null && TurboData.Status.Started.equals(turboData.getStatus())) {
			turboFactor = turboData.turboFactor;
		}
		if (Math.abs(targetSpeed - currentSpeed) < 0.05) {
			// We are near to the target speed, use constant throttle.
			return getConstantThrottleForSpeed(targetSpeed);
		}
		if (targetSpeed > currentSpeed) {
			// We need to accelerate
			double maxAcc = ACCEL_ON_MAX_THROTTLE.eval(currentSpeed, 1.0, turboFactor);
			double maxAccSpeed = currentSpeed + maxAcc;
			if (maxAccSpeed <= targetSpeed) {
				// Even on maximum throttle we won't reach the target speed
				return 1.0;
			}
			double constAcc = getConstantThrottleForSpeed(targetSpeed);
			double accelPercent = (targetSpeed - currentSpeed) / (maxAccSpeed - currentSpeed);
			return lerp(constAcc, 1.0, accelPercent);
		}
		// We need to brake
		double minAcc = ACCEL_ON_MIN_THROTTLE.eval(currentSpeed, 1.0, turboFactor);
		double minAccSpeed = currentSpeed + minAcc;
		if (minAccSpeed >= targetSpeed) {
			// Even on minimum throttle we won't reach the target speed
			return 0.0;
		}
		double constAcc = getConstantThrottleForSpeed(targetSpeed);
		double brakePercent = (currentSpeed - targetSpeed) / (currentSpeed - minAccSpeed);
		return lerp(constAcc, 0.0, brakePercent);
	}
	
	public static double getOptimalTurnSpeed(double radius) {

//		if (Math.abs(radius - 40.0) < 0.1) { return 4.0; }
//		if (Math.abs(radius - 60.0) < 0.1) { return 5.0; }
//		
//		if (Math.abs(radius - 90.0) < 0.1) { return 6.2; } // OK
//		if (Math.abs(radius - 110.0) < 0.1) { return 6.8; } // OK
//		
//		if (Math.abs(radius - 190.0) < 0.1) { return 8.5; }
//		if (Math.abs(radius - 210.0) < 0.1) { return 9.35; }
//		
//		if (Math.abs(radius - 180.0) < 0.1) { return 8.9; } // OK
//		if (Math.abs(radius - 200.0) < 0.1) { return 9.2; } // OK
//		if (Math.abs(radius - 220.0) < 0.1) { return 9.5; } // OK
//		
//		if (1+1 == 2) throw new RuntimeException("RADIUS " + radius);
		
		// www.wolframalpha.com -> quadratic fit {50,4.5},{100,6.5},{200,9.7}
		//double res = -0.0000533333 * radius * radius + 0.048 * radius + 2.23333;
		
		// www.wolframalpha.com -> quadratic fit {40,3.5},{50,4.5},{100,6.7},{200,9.7}
		//double res = -0.000136951 * radius * radius + 0.0704236 * radius + 1.08596;
		
		// quadratic fit {40,3.5},{50,4.3},{100,6.5},{200,9.7}
		//double res = -0.000107001 * radius * radius + 0.0636908 * radius + 1.23734;
		
		// linear fit {40,3.5},{50,4.3},{100,6.5},{200,9.7}
		//double res = 0.0376361 * radius + 2.33048;
		
		double res;
		// quadratic fit {40,4.0},{60,5.0},{90,6.2},{110,6.8},{180,8.9},{200,9.2},{220,9.5}
		res = -0.0000947323 * radius * radius + 0.0551367 * radius + 1.97527;
		
		return res;
	}
	
	private static double lerp(double start, double end, double percent) {
		return start + (end - start) * percent;
	}

}
