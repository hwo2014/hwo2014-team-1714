package olengu.bot.msg;


public class PingMsg extends Msg {
	
	@Override
	protected String getMsgType() {
		return "ping";
	}

	@Override
	public String toString() {
		return "PingMsg []";
	}

}
