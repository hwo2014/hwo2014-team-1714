package olengu.bot.msg;

/**
 * {"msgType": "joinRace", "data": {
 * 		"botId": {
 * 			"name": "keke",
 * 			"key": "IVMNERKWEW"
 * 		},
 * 		"trackName": "germany",
 * 		"carCount": 1
 * 	}}
 */
public class JoinRaceMsg extends Msg {
	
	public static class BotId {
		public final String name;
		public final String key;
		
		public BotId(String name, String key) {
			this.name = name;
			this.key = key;
		}

		@Override
		public String toString() {
			return "BotId [name=" + name + ", key=" + key + "]";
		}
		
	}
	
	public final BotId botId;
	public final String trackName;
	public final int carCount;

	public JoinRaceMsg(BotId botId, String trackName, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}

	@Override
	protected String getMsgType() {
		return "joinRace";
	}

	@Override
	public String toString() {
		return "JoinRaceMsg [botId=" + botId + ", trackName=" + trackName + ", carCount=" + carCount + "]";
	}
	
}
