package olengu.bot.msg;

public class TurboMsg extends Msg {
	
	public final String value;

	public TurboMsg(String message) {
		this.value = message;
	}
	
	@Override
	protected Object getData() {
		return value;
	}

	@Override
	protected String getMsgType() {
		return "turbo";
	}

	@Override
	public String toString() {
		return "TurboMsg [value=" + value + "]";
	}

}
