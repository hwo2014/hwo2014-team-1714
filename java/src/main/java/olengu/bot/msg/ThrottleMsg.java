package olengu.bot.msg;


public class ThrottleMsg extends Msg {
	
	public final double value;

	public ThrottleMsg(double value) {
		this.value = value;
	}

	@Override
	protected Object getData() {
		return value;
	}

	@Override
	protected String getMsgType() {
		return "throttle";
	}

	@Override
	public String toString() {
		return "ThrottleMsg [value=" + value + "]";
	}

}
