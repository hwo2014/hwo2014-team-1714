package olengu.bot.msg;

public class MsgWrapper {
	
	public final String msgType;
	public final Object data;
	public final Long gameTick;

	public MsgWrapper(final Msg msg, Long gameTick) {
		this.msgType = msg.getMsgType();
		this.data = msg.getData();
		this.gameTick = gameTick;
	}

	@Override
	public String toString() {
		return "MsgWrapper [msgType=" + msgType + ", data=" + data + ", gameTick=" + gameTick + "]";
	}
	
}
