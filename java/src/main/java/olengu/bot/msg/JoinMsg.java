package olengu.bot.msg;


public class JoinMsg extends Msg {
	
	public final String name;
	public final String key;

	public JoinMsg(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String getMsgType() {
		return "join";
	}

	@Override
	public String toString() {
		return "JoinMsg [name=" + name + ", key=" + key + "]";
	}

}
