package olengu.bot.msg;

public class SwitchLaneMsg extends Msg {
	
	public enum Direction{ Left, Right }
	
	public final String value;

	public SwitchLaneMsg(Direction direction) {
		this.value = direction.name();
	}
	
	@Override
	protected Object getData() {
		return value;
	}

	@Override
	protected String getMsgType() {
		return "switchLane";
	}

	@Override
	public String toString() {
		return "SwitchLaneMsg [value=" + value + "]";
	}

}
