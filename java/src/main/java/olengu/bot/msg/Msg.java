package olengu.bot.msg;

public abstract class Msg {
	
	protected Object getData() {
		return this;
	}

	protected abstract String getMsgType();

}
