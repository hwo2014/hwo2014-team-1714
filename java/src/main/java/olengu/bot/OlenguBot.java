package olengu.bot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import olengu.bot.ai.AIBase;
import olengu.bot.ai.AIResult;
import olengu.bot.ai.LaneAI;
import olengu.bot.ai.ThrottleAI;
import olengu.bot.ai.TurboAI;
import olengu.bot.model.CarModel;
import olengu.bot.model.MsgHandler;
import olengu.bot.model.RaceModel;
import olengu.bot.msg.JoinMsg;
import olengu.bot.msg.JoinRaceMsg;
import olengu.bot.msg.Msg;
import olengu.bot.msg.MsgWrapper;
import olengu.bot.msg.PingMsg;
import olengu.bot.msg.SwitchLaneMsg;
import olengu.bot.msg.ThrottleMsg;
import olengu.bot.msg.TurboMsg;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class OlenguBot implements Runnable {
	
	private static final boolean DUMP_ALL_MESSAGES = false; // Only for debugging - should be false
	private static final Double FORCED_THROTTLE = null; // Only for testing - should be null

	private static final Gson GSON = new Gson();
	private static final JsonParser JSON_PARSER = new JsonParser();
	
	private final String serverHost;
	private final int serverPort;
	private final String botName;
	private final String botKey;
	private final String trackName;
	private final int carCount;
	
	private Msg lastMsgSent;
	private Long lastMsgSentGameTick;

	public OlenguBot(String serverHost, int serverPort, String botName, String botKey, String trackName, int carCount) {
		this.serverHost = serverHost;
		this.serverPort = serverPort;
		this.botName = botName;
		this.botKey = botKey;
		this.trackName = trackName;
		this.carCount = carCount;
	}
	
	@Override
	public void run() {
		Socket socket = null;
		try {
			System.out.println("*** Olengu Boys robot started ***");
			
			socket = new Socket(serverHost, serverPort);
			socket.setTcpNoDelay(true);
			final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
			
			if (trackName == null) {
				// Needed for continuous integration builds
				sendMsg(writer, new JoinMsg(botName, botKey), null);
			} else {
				// We select the track and the number of opponents
				sendMsg(writer, new JoinRaceMsg(new JoinRaceMsg.BotId(botName, botKey), trackName, carCount), null);
			}
			
			long frameStartMillis, frameTimeMillis;
			Set<AIBase> aiAgents = new LinkedHashSet<AIBase>();
			RaceModel raceModel = new RaceModel();
			Map<String, Set<MsgHandler>> raceModelMsgHandlers = new HashMap<>();
			for (MsgHandler msgHandler : raceModel.getMsgHandlers()) {
				for (String msgType : msgHandler.getHandledMsgTypes()) {
					Set<MsgHandler> msgHandlers = raceModelMsgHandlers.get(msgType);
					if (msgHandlers == null) {
						msgHandlers = new LinkedHashSet<>();
						raceModelMsgHandlers.put(msgType, msgHandlers);
					}
					msgHandlers.add(msgHandler);
				}
			}
			
			String line;
			while((line = reader.readLine()) != null) {
				frameStartMillis = System.nanoTime() / 1000000L;
				if (DUMP_ALL_MESSAGES) { System.out.println("<< " + line); }
				JsonObject msgRx = JSON_PARSER.parse(line).getAsJsonObject();
				String msgRxType = msgRx.get("msgType").getAsString();
				JsonElement data = msgRx.get("data");
				Long gameTick = msgRx.has("gameTick") ? msgRx.get("gameTick").getAsLong() : null;
				
				// Update the race model
				Set<MsgHandler> msgHandlers = raceModelMsgHandlers.get(msgRxType);
				if (msgHandlers == null) {
					System.err.println(gameTick + "| Received an unexpected message type \"" + msgRxType + "\" - details: " + msgRx);
				} else {
					Msg handlerLastMsgSent = gameTick != null && lastMsgSentGameTick != null && lastMsgSentGameTick.longValue() == gameTick.longValue() - 1 ? lastMsgSent : null;
					for (MsgHandler msgHandler : msgHandlers) {
						try {
							msgHandler.onMsg(msgRxType, data, gameTick, handlerLastMsgSent);
						} catch (Exception e) {
							System.err.println(gameTick + "| Exception in message handler for message type \"" + msgRxType + "\"");
							e.printStackTrace();
						}
					}
				}
				
				// Create the AI agents if needed
				if (msgRxType.equals("gameInit")) {
					aiAgents.clear();
					aiAgents.add(new ThrottleAI(raceModel));
					aiAgents.add(new LaneAI(raceModel));
					aiAgents.add(new TurboAI(raceModel));
				}
				
				// Run the AI agents if needed
				if (gameTick != null) {
					switch(msgRxType) {
					case "gameStart":
					case "carPositions":
						if (raceModel.isGameStarted()) {
							// The message received from the server requires a response
						Msg messageToSend;
						final CarModel myCar = raceModel.getMyCar(); 
						if (myCar.isCrashed() || myCar.isFinishReached()) {
								messageToSend = new PingMsg();
							} else if (FORCED_THROTTLE != null) {
								System.out.println(gameTick + "| Throttle - speed: " + myCar.getSpeed() + " angle: " + myCar.getAngle() + " drift-speed: " + myCar.getDriftSpeed());
								messageToSend = new ThrottleMsg(FORCED_THROTTLE);
							} else {
								SortedSet<AIResult> aiResults = new TreeSet<AIResult>();
								// TODO Architecture optimization: we should run all the aiAgents in parallel and then gather the results that we get within a frame time
								for (AIBase aiAgent : aiAgents) {
									try {
										aiAgent.setGameTick(gameTick);
										aiAgent.run();
										AIResult aiResult = aiAgent.getResult();
										if (aiResult != null) { aiResults.add(aiResult); }
									} catch (Exception e) {
										System.err.println(gameTick + "| Exception in AI agent " + aiAgent);
										e.printStackTrace();
									}
								}
								messageToSend = aiResults.isEmpty() ? new PingMsg() : aiResults.first().message;
							}
							sendMsg(writer, messageToSend, gameTick);
							// Update the model to account for the message we have just sent
							try {
								if (messageToSend instanceof ThrottleMsg) {
									ThrottleMsg throttleMsg = (ThrottleMsg) messageToSend;
									for (AIBase aiAgent : aiAgents) {
										if (aiAgent instanceof ThrottleAI) {
											((ThrottleAI) aiAgent).setLastTransmittedThrottle(throttleMsg.value);
										}
									}
								} else if (messageToSend instanceof SwitchLaneMsg) {
									SwitchLaneMsg switchLaneMsg = (SwitchLaneMsg) messageToSend;
									for (AIBase aiAgent : aiAgents) {
										if (aiAgent instanceof LaneAI) {
											System.out.println(gameTick + "| Requesting to switch lane, going " + switchLaneMsg.value);
											((LaneAI) aiAgent).setSelectedDirection(SwitchLaneMsg.Direction.valueOf(switchLaneMsg.value));
										}
									}
								} else if (messageToSend instanceof TurboMsg) {
									TurboMsg turboMsg = (TurboMsg) messageToSend;
									System.out.println(gameTick + "| TURBO! ###### " + turboMsg.value + " ######");
								}
							} catch (Exception e) {
								System.err.println(gameTick + "| Exception in model update after sending message " + messageToSend);
								e.printStackTrace();
							}
						}
						break;
					default:
						// Do nothing
						break;
					}
				}
				
				// Check the processing time
				frameTimeMillis = System.nanoTime() / 1000000L - frameStartMillis;
				if (frameTimeMillis > 1000L / 120L) { // We should reach 120fps, just to be safe 
					System.err.println(gameTick + "| WARNING: slow frame time: " + frameTimeMillis + "ms");
				}
			}
			
			System.out.println("Max drift reached in the whole race: " + raceModel.getMyCar().getMaxAbsAngle());
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (socket != null) { try { socket.close(); } catch (Throwable e) { } }
			System.out.println("*** Olengu Boys robot terminated ***");
		}
	}

	private void sendMsg(PrintWriter writer, Msg messageToSend, Long gameTick) {
		String jsonStr = GSON.toJson(new MsgWrapper(messageToSend, gameTick));
		if (DUMP_ALL_MESSAGES) { System.out.println(">> " + jsonStr); }
		writer.println(jsonStr);
		writer.flush();
		lastMsgSent = messageToSend;
		lastMsgSentGameTick = gameTick;
	}

}
