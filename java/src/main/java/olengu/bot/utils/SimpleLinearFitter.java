package olengu.bot.utils;


public class SimpleLinearFitter implements FunctionFitter {
	
	private static final boolean SHOW_PLOTTER = false; // Only for debugging

	private final String name;
	private final long minSamples;
	private final long maxSamples;
	private final double minSamplesXRange;
	private final Plotter plotter;

	double xMin, yMin, xMax, yMax;
	private long samplesCount = 0;
	private boolean mustUpdate;
	private double slope;
	private double y0;

	public SimpleLinearFitter(String name, long minSamples, long maxSamples, double minSamplesXRange, double initalSlope, double initialY0) {
		this.name = name;
		this.minSamples = minSamples;
		this.maxSamples = maxSamples;
		this.minSamplesXRange = minSamplesXRange;
		slope = initalSlope;
		y0 = initialY0;
		if (SHOW_PLOTTER) {
			plotter = new Plotter(name);
			plotter.setLine(initalSlope, initialY0);
			plotter.show();
		} else {
			plotter = null;
		}
	}

	@Override
	public synchronized boolean addSample(double x, double y) {
		if (samplesCount >= maxSamples) { return false; }
		samplesCount++;
		if (SHOW_PLOTTER) {
			plotter.addPoint(x, y);		
		}
		if (samplesCount == 1) {
			xMin = xMax = x;
			yMin = yMax = y;
		} else {
			double xMed = (xMax - xMin) * 0.5 + xMin;
			double sampleWeight = 1.0 / samplesCount;
			if (x <= xMed) {
				if (x < xMin) { sampleWeight *= 2.0; }
				double invSampleWeight = 1.0 - sampleWeight;
				xMin = xMin * invSampleWeight + x * sampleWeight;
				yMin = yMin * invSampleWeight + y * sampleWeight;
			} else {
				if (x > xMax) { sampleWeight *= 2.0; }
				double invSampleWeight = 1.0 - sampleWeight;
				xMax = xMax * invSampleWeight + x * sampleWeight;
				yMax = yMax * invSampleWeight + y * sampleWeight;
			}
		}
		if (samplesCount >= minSamples && xMax - xMin > minSamplesXRange) {
			mustUpdate = true;
			if (SHOW_PLOTTER) { updateIfNeeded(); }
		}
		return true;
	}

	public synchronized boolean hasEnoughSamples() {
		return samplesCount >= minSamples;
	}

	public synchronized boolean hasAllTheSamples() {
		return samplesCount >= maxSamples;
	}

	@Override
	public synchronized double eval(double x) {
		updateIfNeeded();
        return x * slope + y0;
	}
	
	public synchronized double eval(double x, double slopeMultiplier, double y0Multiplier) {
		updateIfNeeded();
        return x * slope * slopeMultiplier + y0 * y0Multiplier;
	}

	@Override
	public String toString() {
		updateIfNeeded();
		return "SimpleLinearFitter [name=" + name + ", samplesCount=" + samplesCount + ", slope=" + slope + ", y0=" + y0 + "]";
	}

	private synchronized void updateIfNeeded() {
		if (mustUpdate) {
			slope = (yMax - yMin) / (xMax - xMin);
			y0 = yMin - slope * xMin;
			if (SHOW_PLOTTER) {
				plotter.setPoints(xMin, yMin, xMax, yMax);
				plotter.setLine(slope, y0);		
			}
			mustUpdate = false;
			//System.out.println("*** " + name + " updated: " + toString());
		}
	}

}
