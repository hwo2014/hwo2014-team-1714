package olengu.bot.utils;

public interface FunctionFitter {
	
	boolean addSample(double x, double y);
	
	double eval(double x);

}
