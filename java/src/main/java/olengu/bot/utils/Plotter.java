package olengu.bot.utils;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Plotter {
	
	JFrame jFrame;
	DrawPanel drawPanel;
	
	public Plotter(String name) {
		drawPanel = new DrawPanel();
		drawPanel.setBackground(Color.white);
		drawPanel.setPreferredSize(new Dimension(500,300));
		
		jFrame = new JFrame(name);
		jFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		jFrame.getContentPane().add(drawPanel, BorderLayout.CENTER);
		jFrame.pack();
		jFrame.setLocationByPlatform(true);
	}
	
	public void show() {
        jFrame.setVisible(true);
	}
	
	public void addPoint(double x, double y) {
		drawPanel.addPoint(x, y);
		drawPanel.repaint();
	}
	
	public void setPoints(double x1, double y1, double x2, double y2) {
		drawPanel.setPoints(x1, y1, x2, y2);
		drawPanel.repaint();
	}
	
	public void setLine(double slope, double y0) {
		drawPanel.setLine(slope, y0);
		drawPanel.repaint();
	}
	
	private static class DrawPanel extends JPanel {
		private static final long serialVersionUID = 1845303137210460981L;

		private static class Point {
			public double x, y;
		}
		
		private List<Point> points = new CopyOnWriteArrayList<>();
		private volatile double slope, y0, p1x, p1y, p2x, p2y;
		
		public void addPoint(double x, double y) {
			Point point = new Point();
			point.x = x;
			point.y = y;
			points.add(point);
		}
		
		public void setLine(double slope, double y0) {
			this.slope = slope;
			this.y0 = y0;
		}
		
		public void setPoints(double x1, double y1, double x2, double y2) {
			p1x = x1;
			p1y = y1;
			p2x = x2;
			p2y = y2;
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);     
	        g2.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

			final double scaleX = 20.0;
			final double scaleY = 250.0;
			
			g2.setColor(Color.lightGray);
			double x0 = - (getWidth() / 2) / scaleX;
			double x1 = -x0;
			g2.drawLine(0, (int) (getHeight() / 2 - ((x0 * slope) + y0) * scaleY), getWidth(), (int) (getHeight() / 2 - ((x1 * slope) + y0) * scaleY));
			
			g2.setColor(Color.black);
			for (Point point : points) {
				int x = getWidth() / 2 + (int) (point.x * scaleX);
				int y = getHeight() / 2 - (int) (point.y * scaleY);
				g2.drawLine(x, y, x, y);
			}
			
			g2.setColor(Color.green);
			{
				int x = getWidth() / 2 + (int) (p1x * scaleX);
				int y = getHeight() / 2 - (int) (p1y * scaleY);
				g2.drawLine(x - 3, y, x + 3, y);
				g2.drawLine(x, y - 3, x, y + 3);
			}
			g2.setColor(Color.orange);
			{
				int x = getWidth() / 2 + (int) (p2x * scaleX);
				int y = getHeight() / 2 - (int) (p2y * scaleY);
				g2.drawLine(x - 3, y, x + 3, y);
				g2.drawLine(x, y - 3, x, y + 3);
			}
		}
		
	}
	
	public static void main(String[] args) {
		Plotter p = new Plotter("AAA");
		p.show();
		try {
			for (int i = 0; i < 10000; i++) {
				p.addPoint(Math.random() * 10.0, Math.random() * 10.0);
				Thread.sleep(10);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Thread.sleep(100000);
		
	}

}
