package olengu.bot.utils;


public class LeastSquaresLinearFitter implements FunctionFitter {
    
	private final String name;
    private final long minSamples;
    private final long maxSamples;
    private final double minSamplesXRange;
    
    private long samplesCount;
    private double samplesXMin = Double.MAX_VALUE;
    private double samplesXMax = -Double.MAX_VALUE; // Don't dare changing this Double.MIN_VALUE ! It doesn't work like you may think. 
    private double xSum;
    private double ySum;
    private double x2Sum;
    private double xySum;
    private boolean mustUpdate;
    private double slope;
    private double y0;
    
    public LeastSquaresLinearFitter(String name, long minSamples, long maxSamples, double minSamplesXRange, double initalSlope, double initialY0) {
    	this.name = name;
        this.minSamples = minSamples;
        this.maxSamples = maxSamples;
        this.minSamplesXRange = minSamplesXRange;
        slope = initalSlope;
        y0 = initialY0;
    }
    
    public synchronized boolean addSample(double x, double y) {
        if (samplesCount >= maxSamples) { return false; }
        samplesCount++;
        samplesXMin = Math.min(samplesXMin, x);
        samplesXMax = Math.max(samplesXMax, x);
        xSum += x;
        ySum += y;
        x2Sum += x * x;
        xySum += x * y;
        if (samplesCount >= minSamples && samplesXMax - samplesXMin > minSamplesXRange) {
            mustUpdate = true;
        }
        return true;
    }
    
    public synchronized boolean hasEnoughSamples() {
        return samplesCount >= minSamples;
    }
    
    public synchronized boolean hasAllTheSamples() {
        return samplesCount >= maxSamples;
    }
    
    public synchronized double eval(double x) {
        updateIfNeeded();
        return x * slope + y0;
    }
    
    @Override
    public String toString() {
        updateIfNeeded();
        return "LeastSquaresLinearFit [samplesCount=" + samplesCount + ", slope=" + slope + ", y0=" + y0 + "]";
    }
    
    private synchronized void updateIfNeeded() {
        if (mustUpdate) {
            double xMean = xSum / samplesCount;
            double yMean = ySum / samplesCount;
            slope = (xySum - xSum * yMean) / (x2Sum - xSum * xMean);
            y0 = yMean - slope * xMean;
            mustUpdate = false;
            System.out.println("*** " + name + " updated: " + toString());
        }
    }

    public static void main(String[] args) {
    	
    	System.out.println(Byte.MAX_VALUE);
    	System.out.println(Byte.MIN_VALUE);
    	
    	System.out.println(Integer.MAX_VALUE);
    	System.out.println(Integer.MIN_VALUE);
    	
    	System.out.println(Long.MAX_VALUE);
    	System.out.println(Long.MIN_VALUE);
    	
    	
    	
    	System.out.println(Double.MAX_VALUE);
    	System.out.println(Double.MIN_VALUE);
    	
        LeastSquaresLinearFitter lf = new LeastSquaresLinearFitter("Test", 2, 10, 1.0, 0, 0);
        lf.addSample(1.1,2.0);
        lf.addSample(2.3,1.8);
        lf.addSample(-4,3.8);
        System.out.println(lf);
        System.out.println(lf.eval(1.6));
        
        
        SimpleLinearFitter lf2 = new SimpleLinearFitter("Test2", 2, 10, 1.0, 0, 0);
        lf2.addSample(1.1,2.0);
        lf2.addSample(2.3,1.8);
        lf2.addSample(-4,3.8);
        System.out.println(lf2);
        System.out.println(lf2.eval(1.6));
    }

}
