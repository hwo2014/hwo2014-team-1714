package olengu.bot.ai;

import olengu.bot.model.RaceModel;

public abstract class AIBase implements Runnable {
	
	protected final RaceModel raceModel;
	private AIResult aiResult;
	private Long gameTick;
	
	public AIBase(RaceModel raceModel) {
		this.raceModel = raceModel;
	}
	
	public void setGameTick(Long gameTick) {
		this.gameTick = gameTick;
	}
	
	public Long getGameTick() {
		return gameTick;
	}
	
	public AIResult getResult() {
		return aiResult;
	}
	
	@Override
	public void run() {
		aiResult = null;
		aiResult = think();
	}
	
	public abstract AIResult think();

}
