package olengu.bot.ai;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import olengu.bot.model.CarModel;
import olengu.bot.model.PieceModel;
import olengu.bot.model.RaceModel;
import olengu.bot.model.StraightPieceModel;
import olengu.bot.model.TurnPieceModel;
import olengu.bot.msg.ThrottleMsg;

public class ThrottleAI extends AIBase {
	
	private static final DecimalFormat DF = new DecimalFormat("0.###", new DecimalFormatSymbols(Locale.US)); 
	
	private double lastTransmittedThrottle = 0.0; // The last throttle command we actually sent to the server
	
	public ThrottleAI(RaceModel raceModel) {
		super(raceModel);
	}
	
	public void setLastTransmittedThrottle(double lastTransmittedThrottle) {
		this.lastTransmittedThrottle = lastTransmittedThrottle;
	}
	
	private int prevPieceIndex = -1;
	private double maxDrift = 0.0;
	private double maxDriftSpeed = 0.0;
	private double maxBrakingLag = 0.0;
	
	private static String f(double value) {
		return DF.format(value);
	}

	@Override
	public AIResult think() {
		final Long gameTick = getGameTick();
		CarModel myCar = raceModel.getMyCar();

		
		if (myCar.getPieceIndex() != prevPieceIndex) {
			if (prevPieceIndex != -1) {
				// Show stats on piece exit
				System.out.println(gameTick + "| exit max-drift: " + f(maxDrift) + " max-drift-speed: " + f(maxDriftSpeed) + " max-braking-lag: " + f(maxBrakingLag));
			}
			// Reset stats
			maxDrift = 0.0;
			maxDriftSpeed = 0.0;
			// Show stats on piece enter
			PieceModel newPiece = raceModel.getPieceModel(myCar.getPieceIndex());
			System.out.println();
			System.out.println(gameTick + "| " + newPiece + " length: " + f(newPiece.getLength()));
			if (newPiece instanceof TurnPieceModel) {
				TurnPieceModel turnPieceModel = (TurnPieceModel) newPiece;
				double radius = turnPieceModel.radius + raceModel.getLaneDistanceFromCenter(myCar.getEndLaneIndex()) * (-Math.signum(turnPieceModel.angle));
				double pieceTargetSpeed = CarModel.getOptimalTurnSpeed(radius); 
				System.out.println(gameTick + "| enter angle: " + f(myCar.getAngle()) + " speed: " + f(myCar.getSpeed()) + " (" + f(pieceTargetSpeed) + " target) radius: " + radius);
				if (myCar.getSpeed() > pieceTargetSpeed + 0.1) {
					System.err.println(gameTick + "| entering too fast: " + f(myCar.getSpeed() - pieceTargetSpeed));
				}
			} else if (newPiece instanceof StraightPieceModel) {
				System.out.println(gameTick + "| enter angle: " + f(myCar.getAngle()) + " speed: " + f(myCar.getSpeed()));
			}
			prevPieceIndex = myCar.getPieceIndex();
		}
		// Update stats
		maxDrift = Math.max(maxDrift, Math.abs(myCar.getAngle()));
		maxDriftSpeed = Math.max(maxDriftSpeed, myCar.getDriftSpeed());
		
		double targetSpeed = raceModel.getOptimalSpeed(myCar.getPieceIndex(), myCar.getInPieceDistance(), myCar);
		double throttle = myCar.getAdaptiveThrottleForSpeed(myCar.getSpeed(), targetSpeed);
		throttle = Math.min(1.0, Math.max(0.0, throttle));
			
		double importance = Math.abs(lastTransmittedThrottle - throttle);
		if (throttle >= myCar.getConstantThrottleForSpeed(myCar.getSpeed())) {
			importance *= 0.4; // Accelerating is a lot less important than braking!
		}	
		return new AIResult(new ThrottleMsg(throttle), importance);
	}

}

