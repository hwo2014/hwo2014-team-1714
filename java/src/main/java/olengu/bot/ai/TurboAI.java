package olengu.bot.ai;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import olengu.bot.model.CarModel;
import olengu.bot.model.CarModel.TurboData;
import olengu.bot.model.PieceModel;
import olengu.bot.model.RaceModel;
import olengu.bot.model.StraightPieceModel;
import olengu.bot.msg.TurboMsg;

public class TurboAI extends AIBase {
	
	private static final String[] OLENGU_QUOTES = {
		"Porco Tensing!",
		"Ma va a leccartelo e fottiti, t�!",
		"NOLI DEFECARI!",
		"Lampade ti fotto a mente e scendo vittima!",
		"Volete tamponarvi Biiiiaha!!!", 
		"Vuoi recare le mani infanti di riserva Barrichello e l'olofono dei palmipedi sterili!", 
		"Dove l'umpa lumpa lo metti via *boom* e i cani a fare i fessi senti?", 
		"Bhwww bhwohhohhoh���", 
		"Volevo un telaio aiooo!", 
		"Un americano ha bevuto sora nei vari casi!", 
		"Americano blubb�!",
		"Avrete guai fare .... di polpo di cane!", 
		"Pimpa nel luogo del distinguere le frasi Piero gorgi..", 
		"Oouoho! L'han varicato i percussori mentre dumbo di qua libera i baldi..",
		"Bel�n.. era fuori di qui!", 
		"In bici eh?",
		"Americani vado a riporre gli imbuti! Vado a faenz'!", 
		"Oooooh, Gerry Scotti!", 
		"ma ne cadi meno fuori!", 
		"Esci ibernizzato a mano foglia?"
	};
	
	private final double maxStraighLegth;
	private final Map<StraightPieceModel, Double> straightLengthMap = new HashMap<>();  

	public TurboAI(RaceModel raceModel) {
		super(raceModel);
		double tempMaxStraighLegth = 0.0;
		for (int i = 0; i < raceModel.getPieceModelCount(); i++) {
			PieceModel pieceModel = raceModel.getPieceModel(i);
			if (!(pieceModel instanceof StraightPieceModel)) { continue; }
			double straightLegth = pieceModel.getLength();
			for (int j = 1; j < raceModel.getPieceModelCount(); j++) {
				PieceModel pm = raceModel.getPieceModel(i + j);
				if (!(pm instanceof StraightPieceModel)) { break; }
				straightLegth += pm.getLength();
			}
			tempMaxStraighLegth = Math.max(tempMaxStraighLegth, straightLegth);
			straightLengthMap.put((StraightPieceModel) pieceModel, straightLegth);
		}
		maxStraighLegth = tempMaxStraighLegth;
		System.out.println("Max straigth distance: " + maxStraighLegth);
	}

	@Override
	public AIResult think() {
		final Long gameTick = getGameTick();
		CarModel myCar = raceModel.getMyCar();
		if (myCar == null) { return null; }
		
		TurboData turboData = myCar.getTurboData();
		if (turboData == null || !TurboData.Status.Available.equals(turboData.getStatus())) {
			return null;
		}
		
		PieceModel pieceModel = raceModel.getPieceModel(myCar.getPieceIndex());
		if (!(pieceModel instanceof StraightPieceModel)) {
			return null;
		}
		
		Double straightLength = straightLengthMap.get(pieceModel);
		if (straightLength == null) {
			System.err.println(gameTick + "| TurboAI: straight length not found for piece " + pieceModel);
			return null;
		}
		straightLength -= myCar.getInPieceDistance();
		
		double straightLengthPerc = straightLength / maxStraighLegth;
		
		if (straightLengthPerc < 0.6) {
			// Straight length part too short, no turbo
			return null;
		}

		String olenguQuote = OLENGU_QUOTES[ThreadLocalRandom.current().nextInt(OLENGU_QUOTES.length)]; 
		return new AIResult(new TurboMsg(olenguQuote), straightLength * 0.8);
	}

}
