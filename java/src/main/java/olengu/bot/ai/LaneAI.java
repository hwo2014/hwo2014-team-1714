package olengu.bot.ai;

import java.util.Set;

import olengu.bot.model.CarModel;
import olengu.bot.model.PieceModel;
import olengu.bot.model.RaceModel;
import olengu.bot.msg.SwitchLaneMsg;
import olengu.bot.msg.SwitchLaneMsg.Direction;


public class LaneAI extends AIBase {

	public LaneAI(RaceModel raceModel) {
		super(raceModel);
	}
	
	public void setSelectedDirection(SwitchLaneMsg.Direction selectedDirection) {
		CarModel myCar = raceModel.getMyCar();
		myCar.setSelectedDirection(selectedDirection);
	}

	@Override
	public AIResult think() {
		final Long gameTick = getGameTick();
		CarModel myCar = raceModel.getMyCar();
		if (myCar == null) { return null; }
		
		PieceModel currentPiece = raceModel.getPieceModel(myCar.getPieceIndex());
		PieceModel nextPiece = raceModel.getPieceModel(myCar.getPieceIndex() + 1);
		
		if (nextPiece.switchLane) {
			if (myCar.getSelectedDirection() != null) {
				// Already selected a direction
				return null;
			}
		} else {
			// Reset selected direction for any next switch that we will find later
			myCar.setSelectedDirection(null);
			return null; 
		}
		
		// Get the first switch piece
		int switchPieceIndex = myCar.getPieceIndex() + 1;
		
		// Let's do the magic super-intelligent lane selection :)
		Double[] laneCarDistances = new Double[raceModel.getLaneCount()];
		double pieceStartDistance = 0.0;
		for (int pieceIndex = switchPieceIndex; pieceIndex < switchPieceIndex + 4; pieceIndex++) {
			Set<CarModel> carsInPiece = raceModel.getOpponentCarsOnPiece(pieceIndex);
			for (CarModel carModel : carsInPiece) {
				double carDistance = pieceStartDistance + carModel.getInPieceDistance();
				if (laneCarDistances[carModel.getEndLaneIndex()] == null || laneCarDistances[carModel.getEndLaneIndex()].doubleValue() > carDistance) {
					laneCarDistances[carModel.getEndLaneIndex()] = carDistance;
				}
			}
			pieceStartDistance += raceModel.getPieceModel(pieceIndex).getLength();
			boolean allDistancesComputed = true;
			for (Double laneCarDistance : laneCarDistances) {
				if (laneCarDistance == null) {
					allDistancesComputed = false;
					break;
				}
			}
			if (allDistancesComputed) { break; }
		}
		
		final int currentLaneIndex = myCar.getStartLaneIndex();
		final int leftLaneIndex = currentLaneIndex - 1;
		final int rightLaneIndex =  currentLaneIndex + 1;
		
		if (laneCarDistances[currentLaneIndex] == null) {
			// Stay on this lane
			return null;
		}
		
		SwitchLaneMsg.Direction direction = null;
		if (leftLaneIndex >= 0 && laneCarDistances[leftLaneIndex] == null) {
			direction = Direction.Left;
		} else if (rightLaneIndex < laneCarDistances.length && laneCarDistances[rightLaneIndex] == null) {
			direction = Direction.Right;
		} else {
			// Compare distances on all possible lanes
			int bestLane = -1;
			double maxCarDist = -1;
			for (int i = Math.max(0, leftLaneIndex); i < Math.min(laneCarDistances.length - 1, rightLaneIndex); i++) {
				if (laneCarDistances[i] > maxCarDist) {
					maxCarDist = laneCarDistances[i];
					bestLane = i;
				}
			}
			direction = bestLane == leftLaneIndex ? Direction.Left : bestLane == rightLaneIndex ? Direction.Right : null; 
		}
		
		Direction selectedDirection = myCar.getSelectedDirection();
		if (direction == null || selectedDirection != null && selectedDirection.equals(direction)) {
			// Already selected the best lane
			return null;
		}
		
		System.out.println(gameTick + "| Current lane: " + myCar.getStartLaneIndex() + " best direction: " + direction);
		
		double importance = myCar.getInPieceDistance() / currentPiece.getLength();
		importance = Math.max(0.0, Math.min(1.0, importance));
		importance *= 0.7;
		return new AIResult(new SwitchLaneMsg(direction), importance);
		
	}
	

}
