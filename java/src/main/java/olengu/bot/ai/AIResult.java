package olengu.bot.ai;

import olengu.bot.msg.Msg;

public class AIResult implements Comparable<AIResult> {
	
	public final Msg message;
	public final double importance;
	
	public AIResult(Msg message, double importance) {
		this.message = message;
		this.importance = importance;
	}

	@Override
	public int compareTo(AIResult o) {
		return Double.compare(o.importance, importance);
	}

	@Override
	public String toString() {
		return "AIResult [message=" + message + ", importance=" + importance + "]";
	}

}
