package noobbot;

import olengu.bot.OlenguBot;
import olengu.bot.TestValues;

public class Main {

	public static void main(String... args) {
		try {
			String serverHost = args[0];
			int serverPort = Integer.parseInt(args[1]);
			String botName = args[2];
			String botKey = args[3];
			String trackName = args.length >= 5 ? args[4] : null;
			int carCount = args.length >= 6 ? Integer.parseInt(args[5]) : 1;
			
			TestValues.ENABLED = false; //trackName == null; // When you don't specify a track name we use random test values
			
			if (TestValues.ENABLED) {
				System.out.println("*** TEST VALUES ENABLED ***");
				TestValues.dump();
				System.out.println();
			}

			System.out.println("Connecting to " + serverHost + ":" + serverPort + " as " + botName + "/" + botKey);
			OlenguBot olenguBot = new OlenguBot(serverHost, serverPort, botName, botKey, trackName, carCount);
			olenguBot.run();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
}